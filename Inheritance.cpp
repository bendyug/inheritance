// inheritance.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
using namespace std;

class Animal {

public:
    virtual void voice() = 0;
};

class Dog : public Animal {

    void voice() override {
        cout << "Woof \n";
    }
};

class Cat : public Animal {

    void voice() override {
        cout << "Meow \n";

    }
};

class Mouse : public Animal {

    void voice() override {
        cout << "Sqeek \n";
    }
};

int main()
{
    Animal* dog = new Dog();
    Animal* cat = new Cat();
    Animal* mouse = new Mouse();
    Animal** animals = new Animal*[3]{ dog, cat, mouse };
    for (int i = 0; i < 3; i++)
    {
        animals[i]->voice();
    }
    
    delete[] animals;
}
